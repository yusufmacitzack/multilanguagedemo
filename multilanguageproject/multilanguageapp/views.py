from django.shortcuts import render
import json
# Create your views here.
from .models import *
def index(request):
    languages = Languages.objects.all()
    
    item_query = Item.objects.filter(item_id=1)
    item = item_query.values("item_name_TC", "text_description_TC")[0]

    #ITEMIN NAME VE DESCRIPTION TEXTCONTENT ID'LERI
    item_name_tc = item['item_name_TC'] #1
    item_description_tc = item['text_description_TC'] #2

    item_name = TextContent.objects.filter(text_content_id=item_name_tc).values("original_text")[0]['original_text']
    item_description = TextContent.objects.filter(text_content_id=item_description_tc).values("original_text")[0]['original_text']


    if request.method == "POST":
        body = request.POST
        

        language = body['language']
        language_id = Languages.objects.filter(language_name=language).values("language_id")[0]['language_id'] #1
        
        #PANELDEN SEÇİLEN DİLİN DEFAULT LANGUAGE OLUP OLMADIĞINI ANLAMA
        default_language_query = TextContent.objects.filter(text_content_id=1)
        default_language = default_language_query.values("original_language_id")[0]['original_language_id'] #1

        if language_id == default_language: 
            translated_name = TextContent.objects.filter(text_content_id=item_name_tc).values("original_text")[0]['original_text']
            translated_description = TextContent.objects.filter(text_content_id=item_description_tc).values("original_text")[0]['original_text']
        else:
            translated_name = Translations.objects.filter(text_content_id=item_name_tc, language_id=language_id).values("translation")[0]['translation']
            translated_description = Translations.objects.filter(text_content_id=item_description_tc, language_id=language_id).values("translation")[0]['translation']
        return render(request,"index.html" , context={"languages" : languages, "item" : {"item_name" : translated_name,  "item_description" : translated_description }})

    return render(request,"index.html" , context={"languages" : languages, "item" : {"item_name" : item_name,  "item_description" : item_description }})

# {"item_name" : data , "item_description" : "Anything"}