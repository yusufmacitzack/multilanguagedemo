from django.db import models

# Create your models here.
class Item(models.Model): 
    item_id = models.AutoField(primary_key=True)
    item_name_TC = models.IntegerField()
    text_description_TC = models.IntegerField()

class TextContent(models.Model): 
    text_content_id = models.AutoField(primary_key=True)
    original_text = models.CharField(max_length=100)
    original_language_id = models.IntegerField()


class Translations(models.Model): 
    translation_id = models.AutoField(primary_key=True)
    text_content_id = models.IntegerField()
    language_id = models.IntegerField()
    translation = models.CharField(max_length=100)


class Languages(models.Model): 
    language_id = models.AutoField(primary_key=True)
    language_name = models.CharField(max_length=50)

    # def __str__(self): 
    #     return f"{self.language_name}"